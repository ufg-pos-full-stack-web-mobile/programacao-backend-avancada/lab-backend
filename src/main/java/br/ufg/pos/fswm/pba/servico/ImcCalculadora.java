package br.ufg.pos.fswm.pba.servico;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 29/07/17.
 */
public class ImcCalculadora {

    private final Integer peso;
    private final Double altura;

    public ImcCalculadora(Integer peso, Double altura) {
        this.peso = peso;
        this.altura = altura;
    }

    public String getResultado() {
        double imc = calcular();
        String resultado = null;

        if (imc < 17) {
            resultado = "Muito abaixo do peso";
        } else if (imc < 18.49) {
            resultado = "Abaixo do peso";
        } else if (imc < 24.99) {
            resultado = "Peso normal";
        } else if (imc < 29.99) {
            resultado = "Acima do peso";
        } else if (imc < 34.99) {
            resultado = "Obesidade I";
        } else if (imc < 39.99) {
            resultado = "Obesidade II (severa)";
        } else {
            resultado = "Obesidade III (m&oacute;rbida)";
        }

        return resultado;
    }

    private double calcular() {
        return peso / Math.pow(altura, 2);
    }
}
