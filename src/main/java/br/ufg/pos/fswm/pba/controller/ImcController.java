package br.ufg.pos.fswm.pba.controller;

import br.ufg.pos.fswm.pba.servico.ImcCalculadora;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 29/07/17.
 */
@WebServlet(value = "/imc")
public class ImcController extends HttpServlet {

    private ImcCalculadora calculadora;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pesoStr = request.getParameter("peso");
        String alturaStr = request.getParameter("altura");
        String resultado = "";

        if(pesoStr != null && alturaStr != null) {
            int peso = Integer.parseInt(pesoStr);
            double altura = Double.parseDouble(alturaStr) / 100;

            calculadora = new ImcCalculadora(peso, altura);

            resultado = calculadora.getResultado();
        }

        request.setAttribute("resultado", resultado);
        request.getRequestDispatcher("imc-view.jsp").forward(request, response);
    }
}
