<%@ page contentType="text/html; charset=utf-8" %>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Calculadora IMC</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp">
</head>
<body>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3>Calculadora IMC</h3>
                </div>
                <div class="panel-body">
                    <form method="get">
                        <table>
                            <tbody>
                            <tr>
                                <td>Peso:</td>
                                <td><input name="peso" ></td>
                            </tr>
                            <tr>
                                <td>Altura:</td>
                                <td><input name="altura"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><button>Calcular</button></td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                    <b>${resultado}</b>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"></script>
</body>
</html>
